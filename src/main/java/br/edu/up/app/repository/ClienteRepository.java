package br.edu.up.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.edu.up.app.dominio.Cliente;
import br.edu.up.app.dominio.Pessoa;

@RepositoryRestResource(path = "pessoas", excerptProjection = Cliente.class)
public interface ClienteRepository extends CrudRepository<Pessoa, Long> {
	
}
